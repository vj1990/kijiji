GO=$(shell which go)
GOINSTALL=$(GO) install
GOCLEAN=$(GO) clean
GOGET=$(GO) get

# Common
get:
	@echo "Getting Go Packages"
	@$(GOGET) "github.com/labstack/echo"
	@$(GOGET) "go.mongodb.org/mongo-driver/mongo"
	@GO111MODULE=on $(GOGET) "github.com/urfave/cli/v2"
	@echo "Packages Downloaded"


# Docker
BUILDPATH_DOCKER=$(CURDIR)
GOBUILD_DOCKER=CGO_ENABLED=0 GOOS=linux $(GO) build
EXENAME=main

export GOPATH=$(CURDIR)

makedir_docker:
	@echo "Creating directories"
	@if [ ! -d $(BUILDPATH_DOCKER)/bin ] ; then mkdir -p $(BUILDPATH_DOCKER)/bin ; fi
	@if [ ! -d $(BUILDPATH_DOCKER)/pkg ] ; then mkdir -p $(BUILDPATH_DOCKER)/pkg ; fi

build_docker:
	@echo "Building Program"
	$(GOBUILD_DOCKER) -a -installsuffix cgo -ldflags '-extldflags "-static"' -o $(EXENAME) src/main.go
	@echo "Build Finished"

clean_docker:
	@echo "Cleaning All"
	@rm -rf $(BUILDPATH_DOCKER)/bin/$(EXENAME)
	@rm -rf $(BUILDPATH_DOCKER)/pkg
	@rm -rf $(BUILDPATH_DOCKER)/src/github.com

all_docker: makedir_docker get build_docker

# Windows
BUILDPATH_WINDOWS=$(shell pwd)
GOBUILD_WINDOWS=CGO_ENABLED=0 GOOS=windows GOARCH=amd64 $(GO) build
EXENAME=main.exe

export GOPATH=F:/Development/vscode_ws/golang_ws_windows/Base

makedir_windows:
	@echo "Creating directories"
	@if [ ! -d $(BUILDPATH_WINDOWS)/bin ] ; then mkdir -p $(BUILDPATH_WINDOWS)/bin ; fi
	@if [ ! -d $(BUILDPATH_WINDOWS)/pkg ] ; then mkdir -p $(BUILDPATH_WINDOWS)/pkg ; fi

build_windows:
	@echo "Building Program"
	$(GOBUILD_WINDOWS) -a -installsuffix cgo -ldflags '-extldflags "-static"' -o $(EXENAME) src/main.go
	@echo "Build Finished"

clean_windows:
	@echo "Cleaning All"
	@rm -rf $(BUILDPATH_WINDOWS)/bin/$(EXENAME)
	@rm -rf $(BUILDPATH_WINDOWS)/pkg
	@rm -rf $(BUILDPATH_WINDOWS)/src/github.com


all_windows: makedir_windows get build_windows