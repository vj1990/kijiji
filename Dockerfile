FROM golang:alpine as builder
RUN apk add make
RUN apk add git
RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN make all

FROM scratch
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]