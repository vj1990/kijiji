package server

import (
	"net/http"
	"logger"
	"mongo"
	"configParser"

	"github.com/labstack/echo"
)

type ServerSt struct {
	loggerObj	*logger.LoggerSt
	configObj	*configParser.ConfigSt
	mongoObj	*mongo.MongoSt
}

func (serverObj *ServerSt) Login(c echo.Context) error {
	email := c.FormValue("email")
	password := c.FormValue("password")
	authResult := serverObj.mongoObj.ProcessLoginReq(email, password)
	if authResult == true {
		return c.String(http.StatusOK, "Logged In") 
	}
	return c.String(http.StatusOK, "Login Failed")
}

func (serverObj *ServerSt) Register(c echo.Context) error {
	email := c.FormValue("email")
	password := c.FormValue("password")
	username := c.FormValue("username")
	language := c.FormValue("language")

	result := serverObj.mongoObj.ProcessRegistrationReq(username, email, password, language)
	if result == false {
		return c.String(http.StatusOK, "Registration Failed")
	}
	return c.String(http.StatusOK, "Registration Completed")
}

func (serverObj *ServerSt) ShowAllRoutes(c echo.Context) error {
	routes := "No routes yet"
	return c.String(http.StatusOK, routes)
}

func (serverObj *ServerSt) InitRoutes(e *echo.Echo) {
	e.GET("/", serverObj.ShowAllRoutes)
	e.POST("/login", serverObj.Login)
	e.POST("/register", serverObj.Register)
}

func (serverObj *ServerSt) InitServer() {
	serverObj.loggerObj.LogBasic("InitServer", "Initializing Server")

	serverObj.mongoObj = mongo.NewMongo(serverObj.configObj, serverObj.loggerObj)
	err := serverObj.mongoObj.InitMongo()
	if err != nil {
		serverObj.loggerObj.LogBasic("InitServer", "Server Initialization Stopped")
		return
	}

	e := echo.New()
	serverObj.InitRoutes(e)
	e.Logger.Fatal(e.Start(serverObj.configObj.ServerPort))
}

func NewServer(configObj *configParser.ConfigSt, loggerObj *logger.LoggerSt) *ServerSt {
	var server ServerSt
	server.configObj = configObj
	server.loggerObj = loggerObj
	return &server
}