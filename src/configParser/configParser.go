package configParser

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
)

type DatabaseSt struct {
	Name	string	`json:"name"`
	Url	string	`json:"url"`
	TableArticle	string	`json:"tableArticle"`
	TableChatMessage	string	`json:"tableChatMessage"`
	TableCurrency	string	`json:"tableCurrency"`
	TableGroupChat	string	`json:"tableGroupChat"`
	TableUser	string	`json:"tableUser"`
	TableGroupReceiver	string	`json:"tableGroupReceiver"`
	TableLanguage	string	`json:"tableLanguage"`
	TableTag	string	`json:"tableTag"`
	TableUserChat	string	`json:"tableUserChat"`
	TableArticleTag	string	`json:"tableArticleTag"`
	TableFiles	string	`json:"tableFiles"`
	TableTagSubscription	string	`json:"tableTagSubscription"`
}

type DatabaseOperationsSt struct {
	ContextTimeoutFindOne	int	`json:"contextTimeoutFindOne"`
}

type ConfigSt struct {
	ServerPort	string	`json:"serverPort"`
	Database	DatabaseSt	`json:"database"`
	DatabaseOperations	DatabaseOperationsSt	`json:"databaseOperations"`
}

func (config *ConfigSt) parseConfig (filePath string) (error) {
	b, err := ioutil.ReadFile(filePath)
    if err != nil {
		fmt.Print(err)
		return err
	}
	
	err = json.Unmarshal(b, config)
	if err != nil {
		fmt.Print(err)
		return err
	}

	return nil
}

func NewConfig () (*ConfigSt, error) {
	var config ConfigSt
	err := config.parseConfig("config.json")
	if err != nil {
		return nil, err
	}
	return &config, nil
}