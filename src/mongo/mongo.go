package mongo

import (
	"time"
	"context"

	"logger"
	"configParser"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoSt struct {
	mongourl	string
	loggerObj	*logger.LoggerSt
	configObj	*configParser.ConfigSt
	mongoClient	*mongo.Client
}


func (mongoObj *MongoSt) FindOneInCollection(filter bson.M, collectionName string) (*mongo.SingleResult) {
	timeout := mongoObj.configObj.DatabaseOperations.ContextTimeoutFindOne
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(timeout) * time.Second)
	collection := mongoObj.mongoClient.Database(mongoObj.configObj.Database.Name).Collection(collectionName)
	// SingleResult*
	sr := collection.FindOne(ctx, filter)
	if sr.Err() != nil {
		mongoObj.loggerObj.LogBasic("FindOneInCollection", sr.Err().Error())
		mongoObj.loggerObj.LogBasic("FindOneInCollection", "Could not find record matching filter")
		return nil
	}
	return sr
}

func (mongoObj *MongoSt) InsertOneInCollection(collectionName string, newRow interface{}) (error) {
	timeout := mongoObj.configObj.DatabaseOperations.ContextTimeoutFindOne
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(timeout) * time.Second)
	collection := mongoObj.mongoClient.Database(mongoObj.configObj.Database.Name).Collection(collectionName)
	_, err := collection.InsertOne(ctx, newRow)
    if err != nil {
		return err
	}
	return nil
}


func (mongoObj *MongoSt) InitMongo() error {
	mongoObj.loggerObj.LogBasic("InitMongo", "Initializing Mongo")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoObj.configObj.Database.Url))
	if err != nil {
		panic(err)
		return err
	}

	// Ping mongo
	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		panic(err)
		return err
	}

	mongoObj.loggerObj.LogBasic("InitMongo", "Connected to Mongo Db")
	mongoObj.mongoClient = client
	return nil
}

/*New function to instantiate the logger*/
func NewMongo(configObj *configParser.ConfigSt, loggerObj *logger.LoggerSt) *MongoSt {
	var mongoObj MongoSt
	mongoObj.configObj = configObj
	mongoObj.loggerObj = loggerObj
	return &mongoObj
}