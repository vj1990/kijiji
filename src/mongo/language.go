package mongo

import (
	
	//"time"
	//"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type languageTrainer struct {
	ID primitive.ObjectID	`bson:"_id" json:"id,omitempty"`
	Language	string	`json:"language"`
}

func (mongoObj *MongoSt) getLanguageId(language string) (string) {
	var languageT languageTrainer

	filter := bson.M{"language" : language}
	sr := mongoObj.FindOneInCollection(filter, mongoObj.configObj.Database.TableLanguage)
	if sr == nil {
		return ""
	}

	err := sr.Decode(&languageT)
	if err != nil {
		mongoObj.loggerObj.LogBasic("getLanguageId", err.Error())
		return ""
	}

	return languageT.ID.String()
}