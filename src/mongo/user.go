package mongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type userTrainer struct {
	ID primitive.ObjectID	`bson:"_id" json:"id,omitempty"`
	Username	string	`json:"username"`
	Email	string	`json:"email"`
	Language_id	string	`json:"language_id"`
	Password	string	`json:"password"`

}

type userInsertTrainer struct {
	Username	string	`json:"username"`
	Email	string	`json:"email"`
	Language_id	string	`json:"language_id"`
	Password	string	`json:"password"`

}

func (mongoObj *MongoSt) ProcessLoginReq(email	string, password string) bool {
	filter := bson.M{"email" : email, "password" : password}
	sr := mongoObj.FindOneInCollection(filter, mongoObj.configObj.Database.TableUser)
	if sr == nil {
		return false
	}
	mongoObj.loggerObj.LogBasic("ProcessLoginReq", "Authentication Success")
	return true
}

func (mongoObj *MongoSt) GetUserByEmail(email string) (*userTrainer) {
	var userT userTrainer

	filter := bson.M{"email" : email}
	sr := mongoObj.FindOneInCollection(filter, mongoObj.configObj.Database.TableUser)
	if sr == nil {
		return nil
	}

	err := sr.Decode(&userT)
	if err != nil {
		mongoObj.loggerObj.LogBasic("GetUserByEmail", err.Error())
		return nil
	}

	return &userT
}

func (mongoObj *MongoSt) ProcessRegistrationReq(username string, email string, password string, language string) bool {
	languageId := mongoObj.getLanguageId(language)
	if languageId == "" {
		return false
	}

	userT := mongoObj.GetUserByEmail(email)
	if userT != nil {
		mongoObj.loggerObj.LogBasic("ProcessRegistrationReq", "Email already registered")
		return false
	}

	newUser := userInsertTrainer{Username: username, Email: email, Password: password, Language_id: languageId}
	err := mongoObj.InsertOneInCollection(mongoObj.configObj.Database.TableUser, newUser)
	if err != nil {
		mongoObj.loggerObj.LogBasic("ProcessRegistrationReq", err.Error())
		return false
	}
	
	mongoObj.loggerObj.LogBasic("ProcessRegistrationReq", "Registration Success")
	return true
}