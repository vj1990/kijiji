package main

import (	

	"server"
	"logger"
	"configParser"
)

// Main
func main() {
	configObj, err := configParser.NewConfig()
	if err != nil {
		return
	}

	loggerObj := logger.NewLogger()
	loggerObj.SetLogLevel(logger.LOG_LEVEL_MAX)
	loggerObj.LogBasic("main", "Started Kijiji Auto Ad Publisher")

	serverObj := server.NewServer(configObj, loggerObj)
	serverObj.InitServer()
}
