package cli

import (
	"os"
	"log"

	"github.com/urfave/cli/v2"
)

type CliSt struct {
	
}

// CLI
func (*cliObj CliSt) processCli() {
	var app = cli.NewApp()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func NewCli() *CliSt {
	var cli CliSt
	return &cli
}