package logger

import (
	"fmt"
	"log"
)

const (
	LOG_LEVEL_BASIC = iota + 1
	LOG_LEVEL_DEBUG
	LOG_LEVEL_MAX
)

type LoggerSt struct {
	logLevel	int
}

// Internal Functions
func (loggerObj *LoggerSt) LogBasic(funcName string, logStr string) {
	if loggerObj.logLevel >= LOG_LEVEL_BASIC {
		toPrint := fmt.Sprintf("Fun: %s -- Log: %s", funcName, logStr)
		log.Println(toPrint)
	}
}

func (loggerObj *LoggerSt) LogDebug(funcName string, logStr string) {
	if loggerObj.logLevel >= LOG_LEVEL_DEBUG {
		toPrint := fmt.Sprintf("Fun: %s -- Log: %s", funcName, logStr)
		log.Println(toPrint)
	}
}

// Exported Functions
func (loggerObj *LoggerSt) SetLogLevel(logLevel int) {
	loggerObj.logLevel = logLevel
}

/*New function to instantiate the logger*/
func NewLogger() *LoggerSt {
	var loggerObj LoggerSt
	return &loggerObj
}