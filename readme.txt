# Docker Build
docker build -t kijiji . -f Dockerfile

# Docker Run with ports exposed
docker run -d --name myweb -p 1323:1323 -p 22:22 kijiji

# Docker Remove All Images
docker rmi $(docker images -q)

# Curl Commands to Test:
curl -F "email=vjtutor@protonmail.com" -F "password=hahaha" http://localhost:1323/login

curl -F "username=hakka" -F "language=french" -F "email=vj1tutor@protonmail.com" -F "password=abc" http://localhost:1323/register